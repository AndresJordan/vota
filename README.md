# Vota!

# Creador de consultes i enquestes

Un sistema de votació en el qual les enquestes les fa un administrador. Per poder respondre a les diferentes enquestes s'ha d'estar autentificat. El gràfics representen les respostes de tots els usuaris.

## Full de seguiment ##

[Full de càlcul del seguiment](https://drive.google.com/open?id=1VzxGG1S1LkIYnhwNtv9dQIsRXmXjlDLww39BS8a-RzU)

## Directoris ##

- app/ La configuració de l'aplicació, plantilles i traduccions.
    
- bin/ Fitxers executables (per exemple, bin/console).

- src/ El codi PHP del projecte.

- tests/ Proves automàtiques (p.ex. proves unitàries).

- var/ Arxius generats (memòria cau, registres, etc.).

- vendor/ Les dependències de tercers.

- web/ El directori arrel web.

## Prototip ##

[Moqup](https://app.moqups.com/a17edwjorzam@iam.cat/UItNYEOssF/view/page/aa9df7b72)

## LABS ##

[LABS](http://labs.iam.cat/~a17edwjorzam/transversal2/vota/web)

## Manual d'instalació ##

[Manual](https://bitbucket.org/AndresJordan/vota/wiki/Home)

## Futures millores ##


## Autors ##

- Miquel Vidal
- Marc Carranza
- Andrés Jordán
 
