<?php
// src/AppBundle/Form/RespostaForm.php
namespace AppBundle\Form;

use AppBundle\Entity\Resposta;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RespostaForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('valor', ChoiceTpe::class, array(
				'choices' => array(
					'Si' => 1,
					'No' => -1,
				),
			));
	}

}
