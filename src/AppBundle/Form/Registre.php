<?php
// src/AppBundle/Form/Registre.php
namespace AppBundle\Form;

use AppBundle\Entity\Usuari;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class Registre extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => 'Nom d\'usuari', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px']])
            ->add('plainPassword', RepeatedType::class,  ['type' => PasswordType::class,
                'first_options'  => ['label' => 'Contrasenya', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px']],
                'second_options' => ['label' => 'Repeteix la contrasenya', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px']],
            ])
            ->add('save', SubmitType::class, ['label' => 'Registrarse', 'attr' => ['class' => 'btn btn-success', 'style' => 'margin: 10px']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Usuari::class,
        ));
    }
}
