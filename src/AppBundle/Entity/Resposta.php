<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="resposta")
 * @ORM\Entity
 */
class Resposta
{
  
    /**
     * @ORM\ManyToOne(targetEntity="Enquesta")
     * @ORM\JoinColumn(name="enquesta", referencedColumnName="id")
     * @ORM\Id
     */
    private $enquesta;
  
    /**
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumn(name="usuari", referencedColumnName="id")
     * @ORM\Id
     */
    private $usuari;
  
    /**
     * @ORM\Column(name="valor", type="integer", nullable=false)
     */
    private $valor;
  
    /**
     * @Assert\DateTime()
     * @ORM\Column(name="data", type="datetime", nullable=false)
     */
    private $data;

	public function __construct()
	{
		$this->data = new \DateTime();
	}
  
    public function setEnquesta($enquesta)
    {
        $this->enquesta = $enquesta;
    }
  
    public function getEnquesta()
    {
        return $this->enquesta;
    }
  
    public function setUsuari($usuari)
    {
        $this->usuari = $usuari;
    }
  
    public function getUsuari()
    {
        return $this->usuari;
    }
  
    public function setValor($valor)
    {
        $this->valor = $valor;
    }
  
    public function getValor()
    {
        return $this->valor;
    }

	public function setData($data)
	{
		$this->data =  $data;
	}

	public function getData()
	{
		return $this->data;
	}
}
