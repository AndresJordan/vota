<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="enquesta")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\EnquestaRepository")
 */
class Enquesta
{
  
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
  
    /**
     * @ORM\Column(type="string", length=250, nullable=false)
     */
    private $pregunta;
  
    /**
     * @ORM\Column(name="data_inici", type="date", nullable=false)
     */
    private $dataInici;
  
    /**
     * @ORM\Column(name="data_final", type="date", nullable=false)
     */
    private $dataFinal;
  
    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $destacada;
  
    public function getId()
    {
      return $this->id;
    }
  
    public function getPregunta()
    {
        return $this->pregunta;
    }

    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;
    }
  
    public function getDataInici()
    {
        return $this->dataInici;
    }

    public function setDataInici($dataInici)
    {
        $this->dataInici = $dataInici;
    }
  
    public function getDataFinal()
    {
        return $this->dataFinal;
    }

    public function setDataFinal($dataFinal)
    {
        $this->dataFinal = $dataFinal;
    }
  
    public function getDestacada()
    {
        return $this->destacada;
    }

    public function setDestacada($destacada)
    {
        $this->destacada = $destacada;
    }
}