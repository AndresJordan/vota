<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="usuari")
 * @UniqueEntity(fields="username", message="Usuari ja existent")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UsuariRepository")
 */
class Usuari implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 0})
     */
    private $admin;

    public function __construct()
    {
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getId()
    {
      return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getSalt()
    {
      return null;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
      return $this->password;
    }

    public function getAdmin()
    {
      return $this->admin;
    }

    public function setAdmin($admin)
    {
      $this->admin = $admin;
    }

    public function getRoles()
    {
      if ($this->admin == 1) {
        return array('ROLE_ADMIN');
      }
      return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
      //
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
      return serialize(array(
        $this->id,
        $this->username,
        $this->password,
        //$this->salt,
       ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            //$this->salt
        ) = unserialize($serialized);
    }
}
