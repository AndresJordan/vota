<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquesta;
use AppBundle\Entity\Resposta;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\DateTime;

class EnquestaController extends Controller
{

    /**
     * @Route("/", name="index")
     */
    public function selectAllAction(Request $request)
    {

      $enquestes = $this->getDoctrine()
        ->getRepository('AppBundle:Enquesta')
        ->findAll();

      if (count($enquestes)==0) {
        return $this->render('enquestes/content.html.twig', array(
          'message' => 'No hi ha enquestes',
          'enquestes' => null,
          'user' => null,
        ));
      }
      return $this->render('enquestes/content.html.twig', array(
          'message' => null,
          'enquestes' => $enquestes,
          'user' => $this->getUser()->getId(),
      ));
    }

    /**
     * @Route("/crear-enquesta", name="crearEnquesta")
     */
    public function insertAction(Request $request)
    {

      $enquesta = new Enquesta();

      $form = $this->createFormBuilder($enquesta)
        ->add('pregunta', TextType::class, ['label' => 'Pregunta', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px']])
        ->add('data_inici', DateType::class, ['widget' => 'single_text', 'label' => 'Data d\'inici', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px', 'min' => date('Y-m-d')]])
        ->add('data_final', DateType::class, ['widget' => 'single_text', 'label' => 'Data final', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px', 'min' => date('Y-m-d')]])
        ->add('destacada', CheckboxType::class, ['required' => false, 'label' => 'Destacada', 'attr' => ['class' => 'form-control']])
        ->add('save', SubmitType::class, ['label' => 'Crear', 'attr' => ['class' => 'btn btn-success', 'style' => 'margin: 10px']])
        ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($enquesta);
        $em->flush();
        return $this->render('default/form.html.twig', array(
          'message' => 'Enquesta creada correctement',
          'title' => 'Crear Enquesta',
          'form' => $form->createView(),
        ));
      }
      return $this->render('default/form.html.twig', array(
		    'message' => null,
        'title' => 'Crear Enquesta',
        'form' => $form->createView(),
      ));
    }

    /**
     * @Route("/editar-enquesta/{id}", name="editarEnquesta")
     */
    public function updateAction($id, Request $request)
    {

      $em = $this->getDoctrine()->getManager();
      $enquesta = $em->getRepository('AppBundle:Enquesta')->findOneByid($id);

      $form = $this->createFormBuilder($enquesta)
        ->add('pregunta', TextType::class, ['label' => 'Pregunta', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px'], 'disabled' => true])
        ->add('data_inici', DateType::class, ['widget' => 'single_text', 'label' => 'Data d\'inici', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px'/*, 'min' => date('Y-m-d')*/]])
        ->add('data_final', DateType::class, ['widget' => 'single_text', 'label' => 'Data final', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px', 'min' => date('Y-m-d')]])
        ->add('destacada', CheckboxType::class, ['required' => false, 'label' => 'Destacada', 'attr' => ['class' => 'form-control']])
        ->add('save', SubmitType::class, ['label' => 'Actualitzar', 'attr' => ['class' => 'btn btn-success', 'style' => 'margin: 10px']])
        ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $em->flush();
        return $this->render('default/form.html.twig', array(
          'message' => 'Enquesta editada correctament',
          'title' => 'Editar Enquesta',
          'form' => $form->createView(),
        ));
      }
      return $this->render('default/form.html.twig', array(
		'message' => null,
        'title' => 'Editar Enquesta',
        'form' => $form->createView(),
      ));
    }

    /**
     * @Route("/respon-enquesta/{id}", name="responEnquesta")
     */
    public function respostaAction($id, Request $request)
    {
		$pregunta = $this->getDoctrine()
        ->getManager()
        ->createQuery('SELECT e.pregunta FROM AppBundle:Enquesta e WHERE e.id = ' . $id)
        ->getResult();

		$resposta = new Resposta();
		$enquesta = $this->getDoctrine()
			->getRepository(Enquesta::class)
			->findOneById($id);
		$resposta->setEnquesta($enquesta);
		$resposta->setUsuari($this->getUser());
		//$resposta->setData(date('Y-m-d H:i:s'));
		//print_r(date('Y-m-d H:i:s'));
		//$form = $this->createFormBuilder(RespostaForm::class, $resposta);
		$form = $this->createFormBuilder($resposta)
			->setAction($this->generateUrl('responEnquesta', array(
				'id' => $id,
			)))
			->add('valor', ChoiceType::class, ['choices' => ['Si' => 1, 'No' => -1], 'label' => 'Escull una opció', 'attr' => ['class' => 'form-control', 'style' => 'margin: 10px']])
			->add('save', SubmitType::class, ['label' => 'Respon', 'attr' => ['class' => 'btn btn-success', 'style' => 'margin: 10px']])
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
      // Hacemos un merge para que haga un insert o update
			$em->merge($resposta);
			$em->flush();
			return $this->redirectToRoute('index');
		}

		return $this->render('default/form.html.twig', array(
			'message' => null,
			'title' => $pregunta[0]['pregunta'],
			'form' => $form->createView(),
		));
	}
}
