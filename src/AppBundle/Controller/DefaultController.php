<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
// Login
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
// Login - Leftside
use AppBundle\Entity\Enquesta;
use AppBundle\Entity\Resposta;
// Registre
use AppBundle\Form\Registre;
use AppBundle\Entity\Usuari;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {

      // initializes result to null
      $enquestesArray = null;

      // get equestes destacades
      $enquestes = $this->getDoctrine()
        ->getManager()
        ->createQuery('SELECT e.id, e.pregunta FROM AppBundle:Enquesta e WHERE e.destacada = TRUE AND e.dataFinal > CURRENT_DATE() ORDER BY e.dataFinal ASC')
        ->setMaxResults(3)
        ->getResult();

      // checks if there are any
      if (count($enquestes)>0) {
        $enquestesArray = $enquestes;
      }

      // get the login error if there is one
      $error = $authenticationUtils->getLastAuthenticationError();

      // last username entered by the user
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render('login/login.html.twig', array(
        'last_username' => $lastUsername,
        'error'         => $error,
        'title_grafic' => 'Gràfics',
        'destacades' => $enquestesArray,
      ));
    }

    /**
     * @Route("/registrar", name="registre")
     */
    public function registrarAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new Usuari();
        $form = $this->createForm(Registre::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) Usuari no admin
            $user->setAdmin(false);

            // 5) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            //return $this->redirectToRoute('login');
            return $this->render('login/signup.html.twig', array(
				'message' => 'Usuari Creat',
				'form' => $form->createView(),
			)
        );
        }

        return $this->render('login/signup.html.twig', array(
			'message' => null,
			'form' => $form->createView(),
			)
        );
    }


}
