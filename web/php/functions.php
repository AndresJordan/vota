<?php
require_once('conexion.php');
$id = $_POST['enquesta'];
$query = "SELECT COUNT(r.valor) as 'total', COUNT(IF(r.valor=1,1,null)) as 'si', COUNT(IF(r.valor=-1,1,null)) as 'no' FROM enquesta e INNER JOIN resposta r ON e.id=r.enquesta WHERE e.id = " . $id . " ORDER BY e.data_final ASC";
$resultado = mysqli_query($conn, $query);

$respostes = "";

if(!$resultado){
  die("Error");
}else{
  while($data = mysqli_fetch_object($resultado)) {
    $respostes[] = $data;
  }
}

$cod = json_encode($respostes);
echo $cod;

mysqli_free_result($resultado);
mysqli_close($conn);
?>
