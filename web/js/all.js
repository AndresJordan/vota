$(document).ready(init);

function init() {
	// console.log('soy el js de all');
	$('.enquesta').each(function(i, obj) {
		let id = $(this).attr('id').substring(9);
		// console.log(id);
		consultaRespuestas(id);
	});
}

function consultaRespuestas(id) {
	$.ajax({
		async: true,
		type: 'POST',
		data: {enquesta: id},
		url: 'php/functions.php',
		dataType: 'JSON',
		success: function (datos) {
		// console.log("peticion para " + id);
		let resultats = new Array(datos[0].total, datos[0].si, datos[0].no);
		let canvas = 'canvas-enquesta-' + id;
		crearGrafic(canvas, resultats, id);
		},
		error: function (datos) {
			console.log("Error");
		}
	});
}

function crearGrafic(canvas, resultats, id) {
	//console.log(canvas);

	// Asignamos el numero de respuestas totales, si y no
	var pRespostesSi = document.getElementById("resposta-si-" + id);
	if (pRespostesSi) {
		pRespostesSi.innerHTML = resultats[1];
	}
	var pRespostesNo = document.getElementById("resposta-no-" + id);
	if (pRespostesNo) {
		pRespostesNo.innerHTML = resultats[2];
	}
	var pRespostesTotal = document.getElementById("resposta-total-" + id);
	if (pRespostesTotal) {
		pRespostesTotal.innerHTML = resultats[0];
	}
	// Comprobamos que haya datos que mostrar en el gráfico
	cntxEnquesta = document.getElementById("enquesta-" + id);
	if (resultats[0] > 0) {
		let cntxCanvas = document.getElementById(canvas).getContext('2d');
		let chart = new Chart(cntxCanvas, {
			type: 'pie',
			data: {
				labels: ["Si", "No"],
				datasets: [{
					label: "# de vots",
					data: [resultats[1], resultats[2]],
					backgroundColor: [
						'rgba(114, 187, 83, 0.2)',
						'rgba(255, 56, 35, 0.2)'
					]
				}]
			},
			options: {
				scales: {
					yAxis: [{
						tricks: {
							beginAtZero: true
						}
					}]
				}
			}
		});
	} else {
		document.getElementById("canvas-enquesta-" + id).remove();
		cntxEnquesta.innerHTML += '<div class="alert alert-info"><p>Encara no hi ha respostes</p></div>';
	}
}
