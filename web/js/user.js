$(document).ready(init);

function init() {
	// console.log('soy el js de users no admin');
	 var idUsuari = $('#usuari').text();
	$('.enquesta').each(function(i, obj) {
		let idEnquesta = $(this).attr('id').substring(9);
		// console.log(idUsuari + " " + idEnquesta);
		consultaResposta(idUsuari, idEnquesta);
	});

	$(".enquesta").click(function(){
		let id = $(this).attr('id').substring(9);
		// console.log(id);
		getForm(id);
	});
}

function consultaResposta(idUsuari, idEnquesta) {
	$.ajax({
		async: true,
		type: 'POST',
		data: {idUsuari: idUsuari, idEnquesta: idEnquesta},
		url: 'php/functions_user.php',
		dataType: 'JSON',
		success: function (datos) {
			// Comprobamos que se haya respondido
			if (datos[0] != undefined) {
				let respuestaEncuesta = "#icono-respondida-" + datos[0].enquesta;
				// console.log(datos[0].enquesta);
				crearCheckRespondida(respuestaEncuesta);
			}
		},
		error: function (datos) {
			console.log("Error");
		}
	});
}

function crearCheckRespondida(spanRespuestaEncuesta) {
	// Removemos la clase de circulo y añadimos el icono de que si se ha respondido
	$(spanRespuestaEncuesta).removeClass().addClass("glyphicon glyphicon-ok-circle gi-circulo");
	$(spanRespuestaEncuesta).attr("aria-hidden","true");
}



function getForm(id) {
	console.log('se carga el formulario para la enquesta ' + id);
	$.ajax({
		type: 'POST',
		url: 'respon-enquesta/' + id,
		dataType: 'html',
		success: function (datos) {
			console.log("ok");
			$('body').html(datos);
		},
		error: function (datos) {
			console.log("nope");
		}
	});
	return false;
	//$("#resposta_form").html("{{ render(controller('AppBundle:Enquesta:resposta', { 'id': " + id + " })) }}");
}
